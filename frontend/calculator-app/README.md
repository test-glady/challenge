# CalculatorGlady

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.0.

Bonjour,

Je vous remercie de relire mon projet de résoluton du test technique Front et je serais très intéressé d'avoir vos retours.

L'application est développée avec angular 16 et l'éditeur Visual Studio Code.

Il est composé :
 - d'un service api.service pour les appels à l'API
 - d'un dossier model qui comprend les objets CalculorComponent et les classes qui l'implémentent
 - du composant test-calculator qui fait appel :
        - au composant amount-output pour les niveaux 1 et 2 du test
        - au composant calculator-value pour le niveau 3 du test

Le code est développé pour que le niveau 3 soit indépendant du 1 et 2.

Je vous remercie pour le temps consacré à la relecture et espère avoir de vos nouvelles bientôt.

Cordialement

Antoine Chatté

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
