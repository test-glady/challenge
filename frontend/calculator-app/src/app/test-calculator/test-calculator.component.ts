import { Component } from '@angular/core';
import { ApiService } from '../api.service';
import { Ceil } from '../model/ceil';
import { Floor } from '../model/floor';
import { Equal } from '../model/equal';

@Component({
  selector: 'app-test-calculator',
  templateUrl: './test-calculator.component.html',
  styleUrls: ['./test-calculator.component.css']
})
export class TestCalculatorComponent { //  implements OnInit
  
  amount : number | undefined;

  shopId = 5;

  data: any;

  equal: Equal | undefined;

  ceil: Ceil | undefined;

  floor: Floor | undefined;

  constructor(private apiService: ApiService) { };

  chooseNewOutputAmount(amount: number) {

    this.amount = amount;

    this.checkAvailable();

  }

  // For current amount chosen, update items ceil, floor and equal to be displayed in HTML
  checkAvailable() {

    if (this.amount && this.shopId) {

      this.apiService.getResults(this.shopId, this.amount).subscribe(data => 
        {
            this.data = data;
  
            this.data.ceil ? this.ceil = new Ceil(this.data.ceil.value, this.data.ceil.cards) : this.ceil = undefined;
  
            this.data.floor ? this.floor = new Floor(this.data.floor.value, this.data.floor.cards): this.floor = undefined;
  
            this.data.equal ? this.equal = new Equal(this.data.equal.value, this.data.equal.cards) : this.equal = undefined;
  
        }
      );
    }

  }

  // On change of amount from amount-output-component, clear data displayed by items equal, ceil and floor.
  clearData() {

    this.equal = undefined;

    this.ceil = undefined;

    this.floor = undefined;

  }

}
