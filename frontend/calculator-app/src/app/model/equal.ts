import { CalculatorComponentValue } from './calculator-component-value';

export class Equal implements CalculatorComponentValue {

  public name: string;
  public value: number;
  public cards: number[];

    constructor(value: number, cards: number[]) {

      this.name = 'equal';

      this.value = value;

      this.cards = cards;

    }

}