import { CalculatorComponentValue } from './calculator-component-value';

export class Ceil implements CalculatorComponentValue {

  public name: string;
  public value: number;
  public cards: number[];

    constructor(value: number, cards: number[]) {

      this.name = 'ceil';

      this.value = value;

      this.cards = cards;

    }

}