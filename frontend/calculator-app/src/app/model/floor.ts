import { CalculatorComponentValue } from './calculator-component-value';

export class Floor implements CalculatorComponentValue {

  public name: string;
  public value: number;
  public cards: number[];

    constructor(value: number, cards: number[]) {

      this.name = 'floor';

      this.value = value;

      this.cards = cards;

    }

}