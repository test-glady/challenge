import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})

export class ApiService {

    constructor(private http: HttpClient) { }

    getResults(shopId: number, amount: number) {

        const headers = new HttpHeaders().append('Authorization', 'tokenTest123');
        const params = new HttpParams().append('amount', amount);

        return this.http.get('http://localhost:3000/shop/'+shopId+'/search-combination', {headers, params});

    }

}    