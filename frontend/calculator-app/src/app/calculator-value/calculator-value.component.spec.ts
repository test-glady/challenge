import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CalculatorValueComponent } from './calculator-value.component';

describe('CalculatorComponentValueComponent', () => {
  let component: CalculatorValueComponent;
  let fixture: ComponentFixture<CalculatorValueComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CalculatorValueComponent]
    });
    fixture = TestBed.createComponent(CalculatorValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
