import { Component, Input } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { CalculatorComponentValue } from "../model/calculator-component-value";
import { ApiService } from "../api.service";
import { Ceil } from '../model/ceil';
import { Floor } from '../model/floor';

export const minLengthArray = (min: number) => {
    return (c: FormControl): any => {
        if (c.value.length >= min)
            return null;
        
        return { MinLengthArray: true};

    }
}

@Component({
    selector: 'app-calculator-value',
    templateUrl: './calculator-value.component.html',
    styleUrls: ['./calculator-value.component.css']

})

export class CalculatorValueComponent {

    @Input() shopId: number | undefined;

    amount: number | undefined;

    calculatorFormGroup: FormGroup;

    calculatorComponentValue: CalculatorComponentValue | undefined;

    ceil: Ceil | undefined;

    floor: Floor | undefined;
    
    ceilDisabled = true;

    floorDisabled = true;

    constructor(private formBuilder: FormBuilder, private apiService: ApiService) {

        this.calculatorFormGroup = this.formBuilder.group({
            value: [,Validators.required],
            cards: [[], minLengthArray(1)]
        });        

    }

    get cards(): number[]  {

        return this.calculatorFormGroup.value.cards;
        
    }

    ngOnInit() {

        // on calculatorForm value change find cards available with API
        this.calculatorFormGroup.valueChanges.subscribe(calculatorFormVal => {

            this.calculatorComponentValue = undefined;

            this.floorDisabled = true;

            this.ceilDisabled = true;

            if (calculatorFormVal.value != this.amount) {

                this.amount = calculatorFormVal.value;
    
                this.floor = undefined;
    
                this.ceil = undefined;
                
                this.findCards();
    
                this.checkPreviousAndNextAmount();

            }

        });

    }


    // Save values of the form
    saveCalculatorValues(): void {

        this.calculatorComponentValue = this.calculatorFormGroup.value;

    }

    // Find available cards with current value
    findCards() {

        if(this.shopId && this.amount) {

            this.apiService.getResults(this.shopId, this.amount).subscribe(data => 
                {
                    const dataApi: any = data;
        
                    if (dataApi.equal) {

                        // if current value is available with cards : update form with cards from API response
                        this.calculatorFormGroup.patchValue({cards: dataApi.equal.cards});
        
                    } else {

                        // else find close amounts available with cards from API response
                        this.calculatorFormGroup.patchValue({cards: []});

                        this.findCloseAmount(dataApi);

                    }
          
                }

              );

        }

    }    

    // Find close amounts and update ceil and floor to be displayed in HTML
    findCloseAmount(dataApi: any) {

        dataApi.ceil ? this.ceil = new Ceil(dataApi.ceil.value, dataApi.ceil.cards) : this.ceil = undefined;
  
        dataApi.floor ? this.floor = new Floor(dataApi.floor.value, dataApi.floor.cards): this.floor = undefined;

    }

    // Update amount
    chooseNewAmount(amount: number) {

        this.calculatorFormGroup.patchValue({value: amount});

    }

    // One method for both "Montant suivant" and "Montant précédent" buttons - then update amount
    findNextOrPreviousAmount(belowOrHigher: number) {

        if (this.amount && this.shopId) {

            const newAmount = this.amount + belowOrHigher;

            this.apiService.getResults(this.shopId, newAmount).subscribe(data => 
                {
                    const dataApi: any = data;
        
                    if (dataApi.equal) {

                        this.chooseNewAmount(dataApi.equal.value)
        
                    } else {

                        belowOrHigher > 0 ? this.chooseNewAmount(dataApi.ceil.value) : this.chooseNewAmount(dataApi.floor.value);

                    }
          
                }

              );

        }

    }
    

    // This method check if buttons "Montant suivant" and "Montant précédent" are disabled or not with current amount
    checkPreviousAndNextAmount() {

        if (this.amount && this.amount > 0) {

            // for current amount, do API call with next and previous value (amount -/+ 1) to disable button or not.
            const nextVal = this.amount + 1;
            const previousVal = this.amount - 1;

            if (this.shopId) {

                this.apiService.getResults(this.shopId, nextVal).subscribe(data => {

                    const checkData: any = data;
            
                    checkData.ceil ? this.ceilDisabled = false : this.ceilDisabled = true;
            
                });
            
                this.apiService.getResults(this.shopId, previousVal).subscribe(data => {
            
                    const checkData: any = data;
            
                    checkData.floor ? this.floorDisabled = false : this.floorDisabled = true;
            
                });  

            }

        }

    }      

}
