import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountOutputComponent } from './amount-output.component';

describe('AmountOutputComponent', () => {
  let component: AmountOutputComponent;
  let fixture: ComponentFixture<AmountOutputComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AmountOutputComponent]
    });
    fixture = TestBed.createComponent(AmountOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
