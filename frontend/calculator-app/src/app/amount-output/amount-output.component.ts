import { Component, Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { ApiService } from '../api.service';
import { Ceil } from '../model/ceil';
import { Floor } from '../model/floor';
import { Equal } from '../model/equal';

@Component({
  selector: 'app-amount-output',
  templateUrl: './amount-output.component.html',
  styleUrls: ['./amount-output.component.css']
})
export class AmountOutputComponent { // implements OnChanges   

  @Input() amount : number | undefined;

  @Input() shopId: number | undefined;

  @Output() desiredAmountEvent = new EventEmitter<number>();

  @Output() changeEvent = new EventEmitter();

  equal: Equal | undefined;

  ceil: Ceil | undefined;

  floor: Floor | undefined;

  nextData: any;

  previousData: any;

  ceilDisabled = true;

  floorDisabled = true;

  constructor(private apiService: ApiService) { };

  // One method for both "Montant suivant" and "Montant précédent", 
  findCloseAmount(belowOrHigher: number) {

    if (this.amount && this.shopId) {

      const newAmount = this.amount + belowOrHigher;

      this.apiService.getResults(this.shopId, newAmount).subscribe(data => {

        const newData: any = data;

        if (newData.equal) {

          this.amount = newAmount;

          // check if same buttons should be disabled or not
          this.valueChange(this.amount);

        } else {

          belowOrHigher > 0 ? this.amount = newData.ceil.value : this.amount = newData.floor.value;

          // check if same buttons should be disabled or not
          if (this.amount) this.valueChange(this.amount);

        }

      });
    }

  }

  // On value change, clear data and call API to check if buttons "Montant suivant" and "Montant précédent" must be disabled or not.
  valueChange(val: number) {

    // Send an event to clear data displayed in test-talculator-component
    this.changeEvent.emit(val);

    const nextVal = val + 1;
    const previousVal = val - 1;

    if (this.shopId && val > 0) {

      this.apiService.getResults(this.shopId, nextVal).subscribe(data => {

        const checkData: any = data;
  
        checkData.ceil ? this.ceilDisabled = false : this.ceilDisabled = true;
  
      });
  
      this.apiService.getResults(this.shopId, previousVal).subscribe(data => {
  
        const checkData: any = data;
  
        checkData.floor ? this.floorDisabled = false : this.floorDisabled = true;
  
      });  

    }

  }
  
  // on click Validate button, update the amount and send event to parent
  chooseDesiredAmount(amount: string) {

    this.amount = parseInt(amount);

    this.desiredAmountEvent.emit(this.amount);

  }

}
